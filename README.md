## Exercise 1:

Create two different themes applying to the form, considering the theme modifier class. And also add a variant for 2 kinds of fonts, normal and big (based on your criteria).

You can use the styles.css to do your work, I'm going to evaluate only the stylesheet. Please create a branch with your name and work with it.

In this exercise, you'll practice how to apply styles to specific elements using BEM methodology. Using common properties like borders, backgrounds, colors and fonts.

---

## Points to evaluate

* Themes should have the same look between browsers.
* Add some margin between .form__input-group divs.
* Apply styles when a user has selected some field.
* Apply styles to buttons when the mouse is over the button, when it's clicked and when it has a disabled state. Consider the 'btn' block, which can be applied to anchor and button tag.
* Add styles to form messages, depending of their context (correct and wrong field).
