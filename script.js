!function (doc) {
    /*
     * Form Interaction
     */
    function Form() {
        /*
         * @constructor
         */
        const form =  doc.getElementById('main-form'),
            fields = doc.querySelectorAll('#main-form input, #main-form textarea, #main-form select'),
            submitBtn = doc.getElementById('btn-submit'),
            clearBtn = doc.getElementById('clearMsgs');

        var self = this,
            themeSelected = 'form--default',
            fontSelected = 'form--font-normal';

        setEvents();


        /*
         * Set buttons events
         * @private
         */
        function setEvents() {
            /* Theme Modifiers */
            const themeBtns = doc.querySelectorAll('.btn-controls .btn--theme-mod');

            for (let i = 0; i < themeBtns.length; i++) {
                const el = themeBtns[i];

                el.addEventListener('click', setTheme);
            }

            /* Submit Form */
            submitBtn.addEventListener('click', function(ev) {
                ev.preventDefault();
                self.submit();
            });

            /* Clear Form */
            clearBtn.addEventListener('click', function(ev) {
                ev.preventDefault();
                clearMsgs();
            });
        }


        /**
         * Set Modifier of btn clicked
         * @param {Object} btn - btn clicked
         * @private
         */
        function setTheme() {
            if(!!this.dataset.theme) {
                var newTheme = this.dataset.theme;

                form.classList.replace(themeSelected, newTheme);
                themeSelected = newTheme;
            }
            else {
                var newFont = this.dataset.font;

                form.classList.replace(fontSelected, newFont);
                fontSelected = newFont;
            }
        }


        /**
         * Clear Messages
         * @private
         */
        function clearMsgs() {
            for (let i = 0; i < fields.length; i++) {
                const el = fields[i],
                    parent = el.parentElement;
                var msg = parent.getElementsByClassName('form__message')[0]

                if(!!msg) {
                    msg.remove();
                    parent.classList.remove('form__input-group--correct');
                    parent.classList.remove('form__input-group--wrong');
                }
            }
        }


        /**
         * Show error message
         * @private
         * @param {Object} field - field to add msg
         */
        function errorMsg(field) {
            console.log(field.parentElement);
        }


        /**
         * Create div message
         * @private
         * @param {String} string - content message
         * @return {Object} div element
         */
        function createMsg(string) {
            var msg = doc.createElement('div');
            msg.className = 'form__message';
            msg.innerHTML = string;

            return msg;
        }


        /**
         * Show ok message
         * @private
         * @param {Object} field - field to add message
         */
        function okMsg(field) {
            var parent = field.parentElement, 
                msg = createMsg('good!');
            
            parent.classList.add('form__input-group--correct');
            parent.appendChild(msg);
        }


        /**
         * Show error message
         * @private
         * @param {Object} field - field to add message
         */
        function errorMsg(field) {
            var parent = field.parentElement, 
                msg = createMsg('the field is empty');
            
            parent.classList.add('form__input-group--wrong');
            parent.appendChild(msg);
        }


        /**
         * Validate Fields
         * @public
         */
        this.validate = function() {
            for (let i = 0; i < fields.length; i++) {
                const el = fields[i];

                if(!!el.value.length) {
                    okMsg(el);
                }
                else {
                    errorMsg(el);
                }
            }
        };


        /**
         * Submit action
         * @public
         */
        this.submit = function() {
            clearMsgs();
            this.validate();
        };
    }

    doc.addEventListener('DOMContentLoaded', () => {
        var form = Form();
    });
}(document);